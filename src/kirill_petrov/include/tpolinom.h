#pragma once
#include "theadring.h"
#include "tmonom.h"
#include <iostream>
#include <string>
#include <boost/lexical_cast.hpp>
#include <regex>
#include <exception>

using namespace std;
using boost::lexical_cast;

enum Variables {z = 1, y  = 10, x = 100};

class TPolinom : public THeadRing {
public:
	TPolinom(int monoms[][2] = NULL, int km = 0); // �����������
	// ������������ ������
												  // �������� �� ������� ������������-������
	TPolinom(TPolinom &q);      // ����������� �����������
	PTMonom  GetMonom() { return (PTMonom)GetDatValue(); }
	TPolinom operator+(TPolinom &q);   // �������� ���������
	TPolinom & operator=(TPolinom &q); // ������������

	// ����������� ������
	TPolinom Derivative(Variables v = z);					// ���������� �����������
	bool operator==(TPolinom &q);							// ��������� ���������
	friend ostream & operator<<(ostream &os, TPolinom &q);	// �����
	friend istream & operator>>(istream &os, TPolinom &q);	// ����
};