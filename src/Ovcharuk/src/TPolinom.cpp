//Copyright 2016 Ovcharuk Oleg

#include "TPolinom.h"
#include "TMonom.h"
#include <iostream>
#include <string>
#include <regex>
#include "boost/lexical_cast.hpp"

using namespace std;
using namespace boost;

TPolinom::TPolinom(int monoms[][2], int km) 
{
	PTMonom elem = new TMonom(0, -1);
	pHead->SetDatValue(elem);
	for (int i = 0; i < km; i++)
	{
		elem = new TMonom(monoms[i][0], monoms[i][1]);
		InsLast(elem);
	}
}

TPolinom::TPolinom(TPolinom &q)
{
	PTMonom elem = new TMonom(0, -1);
	pHead->SetDatValue(elem);
	for (q.Reset(); !q.IsListEnded(); q.GoNext())
	{
		elem = q.GetMonom();
		InsLast(elem->GetCopy());
	}
	q.Reset();
}

TPolinom TPolinom::operator+(TPolinom &q) 
{
	TPolinom result;
	
	Reset();
	q.Reset();

	TMonom nul(0, -1);
	TMonom elem1(0,0);
	TMonom elem2(0,0);
	PTMonom tmp = NULL;

	while ((!IsListEnded()) || (!q.IsListEnded())){
		if (IsListEnded())
			elem1 = nul;
		else
			elem1 = *GetMonom();

		if (q.IsListEnded())
			elem2 = nul;
		else
			elem2 = *q.GetMonom();

		if (elem1 < elem2){
			tmp = new TMonom(elem2.GetCoeff(), elem2.GetIndex());
			q.GoNext();
		}
		else if (elem2 < elem1){
			tmp = new TMonom(elem1.GetCoeff(), elem1.GetIndex());
			GoNext();
		}
		else if (elem1.GetIndex() == elem2.GetIndex()){
			tmp = new TMonom(elem1.GetCoeff() + elem2.GetCoeff(), elem1.GetIndex());
			GoNext();
			q.GoNext();
		}

		if (tmp->GetCoeff())
			result.InsLast(tmp);
	}
	return result;
}

TPolinom & TPolinom:: operator=(TPolinom &q)
{
	DelList();
	if ((&q != NULL) && (&q != this))
	{
		PTMonom Mon = new TMonom(0, -1);
		pHead->SetDatValue(Mon);
		for (q.Reset(); !q.IsListEnded(); q.GoNext())
		{
			Mon = q.GetMonom();
			InsLast(Mon->GetCopy());
		}
	}
	return *this;
}

TPolinom TPolinom::Diff(variables xyz){ 

	TPolinom result;

	PTMonom elem = new TMonom(0,-1);
	int index;
	int tmp=0;
	for (Reset(); !IsListEnded(); GoNext()){

		index = GetMonom()->GetIndex();

		switch (xyz){
		case x:
			tmp = index / 100;
			break;
		case y:
			tmp = (index % 100) / 10;
			break;
		case z:
			tmp = index % 10;
			break;
		default:
			break;
		}

		if (tmp)
		{
			elem = new TMonom(GetMonom()->GetCoeff() * tmp, index - xyz);
			result.InsLast(elem);
		}
	}
	return result;
}

TPolinom TPolinom::Integr(variables xyz){ 

	TPolinom result;

	PTMonom elem = new TMonom(0, -1);
	int index;
	int tmp = 0;
	for (Reset(); !IsListEnded(); GoNext()){

		index = GetMonom()->GetIndex();

		switch (xyz){
		case x:
			tmp = index / 100;
			break;
		case y:
			tmp = (index % 100) / 10;
			break;
		case z:
			tmp = index % 10;
			break;
		default:
			break;
		}

		if (tmp<9)
		{
			elem = new TMonom((GetMonom()->GetCoeff()) / (tmp+1), index + xyz);
			result.InsLast(elem);
		}
	}
	return result;
}

double TPolinom::Calc(int x, int y, int z){ 
	double result = 0;
	int coef, ind = 0;
	int indx, indy, indz = 0;
	for (Reset(); !IsListEnded(); GoNext()){
		coef = GetMonom()->GetCoeff();
		ind = GetMonom()->GetIndex();
		indx = ind / 100;
		indy = (ind % 100) / 10;
		indz = ind % 10;
		result += coef*pow(x, indx)*pow(y, indy)*pow(z, indz);
	}
	return result;
}

ostream & operator<<(ostream & os, TPolinom & q){ 

	string p_out = "";
	string buf = "";
	string sign;

	int coef, ind, indx, indy, indz = 0;

	for (q.Reset(); !q.IsListEnded(); q.GoNext()){
		coef = q.GetMonom()->GetCoeff();
		ind = q.GetMonom()->GetIndex();
		indx = ind / 100;
		indy = (ind % 100) / 10;
		indz = ind % 10;

		sign = "+";
		if (coef < 0)
			sign = "-";
		buf = to_string(abs(coef));

		if (indx)
			if (indx != 1)
				buf = buf + "x^" + to_string(indx);
			else
				buf = buf + "x";
		if (indy)
			if (indy != 1)
				buf = buf + "y^" + to_string(indy);
			else
				buf = buf + "y";
		if (indz)
			if (indz != 1)
				buf = buf + "z^" + to_string(indz);
			else
				buf = buf + "z";

		if (p_out != "")
			p_out = p_out + " " + sign + " " + buf;
		else{
			if (sign == "-")
				p_out += sign;
			p_out += buf;
		}
	}
	os << p_out << endl;;
	return os;
}

typedef regex_iterator<string::iterator> iter;

istream & operator>>(istream & os, TPolinom & q) // Copyright 2016 Kirill Petrov (������ �������)
{
	// ��������������� ����������
	int i = 0, coeff, index;
	string buf;

	// ���� "��������"
	os >> buf;

	PTMonom mon;
	// ��������� ��������� �� ���������� ������� ������
	regex re_input("(([\\+|-]|^)[1-9](\\d+)?x\\^\\dy\\^\\dz\\^\\d)+");
	// �������� ��� ����������� � ������
	regex re_coeff("^\\d+|(\\+|-)((\\d+))");
	// �������� ��� ������� �� ^
	regex re_index("(\\^\\d)");
	// ��� ���������, �� ������ ��������� ���������� ���������
	iter it_index(buf.begin(), buf.end(), re_index);
	iter it_coeff(buf.begin(), buf.end(), re_coeff);
	// �������� ��� ���������� ��������� � ������ �� ���� ������ �����
	for (; it_coeff != iter(); ++it_coeff, ++i)
	{
		// �������� ������������
		coeff = lexical_cast <int>(it_coeff->str());
		// ������� ����������� ������ ������ x*100+y*10+z*1
		index = lexical_cast <int>(it_index++->str()[1]) * 100 +  //x
			lexical_cast <int>(it_index++->str()[1]) * 10 +       //y
			lexical_cast <int>(it_index++->str()[1]);             //z
		// ������ ����� � ������� � ����� ������
		mon = new TMonom(coeff, index);
		q.InsLast(mon);
	}
	return os;
}
