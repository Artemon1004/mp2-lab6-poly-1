//Copyright 2016 Ovcharuk Oleg

#include "menu.h"
#include <locale>

using namespace std;

int main(){
	setlocale(LC_ALL, "rus");
	Menu main_menu;
	main_menu.start();
	return 0;
}
